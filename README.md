# README #

2020/12/01 作成

## Install ##
---

Install with npm:

`npm install core-js`

webpack関連

`npm install -D @babel/core @babel/preset-env babel-loader webpack webpack-cli webpack-dev-server`

html, ejs関連

`npm install -D html-loader ejs-plain-loader html-webpack-plugin ejs globule`

css関連

`npm install -D sass sass-loader css-loader postcss-loader postcss-sort-media-queries mini-css-extract-plugin webpack-fix-style-only-entries autoprefixer`

## Introduction ##
---

### JS ###

Node, jQuery

### CSS ###

FLOCSS

- [hiloki / flocss](https://github.com/hiloki/flocss)

- [[CSS設計] 私のためのFLOCSSまとめ](https://qiita.com/super-mana-chan/items/644c6827be954c8db2c0)

### HTML ###

ejs

## Usage ##
---

初回のみ

`npm install`

ビルド

`npm run publish`

~~htmlがminifyされて出力されるので、vscodeのFormat整形をする。~~
