///////////////////////////////////////////////////////////////////////////////////
　　//////////////////////////// 　　文字挿入　　　////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

// ブランド略称
// NS: 	str / cym
// MSK: cym / frt
// ONC: alm / ram => lilly
// ONC: abe
// DMG: tlc / gly / hlg /hmt / trz / jad / baq / lum => lilly
// DMG: baq / gly / lum / tlc / jad / trd / INS
// AI: tal / olm
// ON: emg / rey / frt
var PRODUCT = 'baq';

// イベントID（URLから取得）
// yymmdd + ブランド略称
var EVENTID = 'jcwdmgbaq';

// GAタグ用prefix / AUTO
var PREFIX = 'veeva_vod' + EVENTID;

//「いいえ」のリンク先
var NOLINK = 'https://www.lilly.co.jp/';

//「電子添文」のリンク先　（未入力の場合は、電子添文ボタンが非表示になる）
// var DOCUMENT = 'https://www.lillymedical.jp/jp/ja/ra/olumiant/index.aspx';
var DOCUMENT = 'https://test.com';

// PDFファイル名(PDFページの場合)
var PDFFILENAME = 'sample.pdf';

//  演題
var TITLE01 = '製剤バクスミコンテンツ';

//  副題
var TITLE02 = '';

//  EQ MID - Normal
var MOVIEVOD = '424';

//  EQ MID - High Speed
var HSMOVIEVOD = '424';


///////////////////////////////////////////////////////////////////////////////////
/////
///// 自動生成
/////
var PDFFILEPATH = '../' + PDFFILENAME;
var isTargetPageRegex  = /(movie|pdf)/g;

// ページタイトル <title></title>
if(document.URL.match(isTargetPageRegex)) {
  if (TITLE01.indexOf('<br>') !== -1 || TITLE02.indexOf('<br>') !== -1) {
    DOCTITLE = TITLE01.replace("<br>", "");
    if (TITLE02 != ''){
      DOCTITLE += ' ' + TITLE02.replace("<br>", "");
    }
  } else {
    DOCTITLE = TITLE01 + ' ' + TITLE02;
  }
  document.title = DOCTITLE;
}

// ロゴパス / AUTO
if (PRODUCT == 'alm' || PRODUCT == 'gly' || PRODUCT == 'hlg' || PRODUCT == 'hmt' || PRODUCT == 'trz'){
  var LOGOPC = '<img src="../../../common_dmg/images/logo_lilly.png" alt="">';
} else {
  var LOGOPC = '<img src="../../../common_dmg/images/logo_' + PRODUCT + '.png" alt="">';
}

// フッター共通
var FOOTERLOGO = '<img src="../../../common_dmg/images/logo_lilly.png" alt="Lilly"/>';
var COPYRIGHT = 'Copyright © 2023 Eli Lilly Japan K.K. All rights reserved';

// GAイベントラベル
var EVENTLABELNOLINK = 'ga(\'send\', \'event\', \'Outbound\', \'Click\', \'' + NOLINK + '_' + '_' + EVENTID + '\');';
var EVENTLABELPRECAUTIONS = 'ga(\'send\', \'event\', \'Inbound\', \'Click\', \'attention/index.html_' + '_' + EVENTID + '\');';

$(function() {
	//-----------------------------
	// yes_no.html
	//-----------------------------
	// いいえボタンのリンク
	$('.nolink').attr('href', NOLINK);

	// GAイベントラベル
	$('.nolink').attr('onClick', EVENTLABELNOLINK);
	$('.precautions').attr('onClick', EVENTLABELPRECAUTIONS);


	//-----------------------------
	// player
	//-----------------------------
	// 動画タイトル
	$('#videoTitle01').html(TITLE01);
	$('#videoTitle02').html(TITLE02);

	// ブランドロゴ
	$('#brandLogo').html(LOGOPC);

	switch($("body").data("pageType")){
		case "movie":
			// 通常再生mid
			$('#eqPlayer').attr('data-normal-speed-mid', MOVIEVOD);

			// 倍速再生が必要な場合のみ表示
			$('#speedChange').hide();
			if (HSMOVIEVOD != ''){
				$('#speedChange').show();
				$('#eqPlayer').attr('data-high-speed-mid', HSMOVIEVOD);
			};

			// 電子添文がある場合のみ表示
			$('#document').hide();
			if (DOCUMENT != ''){
				$('#document').html('<a href="' + DOCUMENT + '" target="_blank" class="tacosLogEvent" data-eventcd="document" onClick="ga(\'send\', \'event\', \'Outbound\', \'Click\', \'' + DOCUMENT + '_' + '_' + EVENTID + '\');">電子添文情報を見る</a>');
				$('#document').show();
			};
			break;

		case "pdf":
			// PDFダウンロード
			$('#displayPdf').hide();
			$('#downloadPdf').hide();
			if(PDFFILENAME != ''){
				$('#displayPdf').html('<a href="#pdfModal" rel="modal:open" id="displayPdfButton">PDFを表示</a>');
				$('#displayPdf').show();
				$("#displayPdfButton").click();
				$('#downloadPdf').html('<a href="'+ PDFFILEPATH +'" download="'+PDFFILENAME+'" >ダウンロード</a>');
				$('#downloadPdf').show();
				$('#pdfIframe').attr('src', function() {
					let s = $(this).attr('src');
					return s + "?file=../../../../event/" + EVENTID + "/" + PDFFILENAME;
				})
			}
			break;
	}
  // DMG領域のみフッターロゴの表示非表示
  // ONC: alm
  // DMG: gly / hlg / hmt / trz
  if (PRODUCT == 'alm' || PRODUCT == 'gly' || PRODUCT == 'hlg' || PRODUCT == 'hmt' || PRODUCT == 'trz'){
    $('#footerLogo').hide();
  }

  // フッター共通
  if(document.URL.match(isTargetPageRegex)) {
    $('#footerLogo').html(FOOTERLOGO);
  }
  $('#copyright').html(COPYRIGHT);
});


// Google Analytics
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-69864277-7', 'auto'); //IDを更新
ga('send', 'pageview');



///////////////////////////////////////////////////////////////////////////////////
/////
///// viewport.js
/////

(function() {

	var isThis = new function() {
		this.ua = navigator.userAgent;

		this.iPhone = this.ua.indexOf("iPhone") != -1;
		this.iPad = this.ua.indexOf("iPad") != -1;
		this.iOS = this.iPhone || this.iPad;

		this.Android = this.ua.indexOf("Android") != -1;
		this.Android2 = this.ua.indexOf("Android 2") != -1;
		this.Android4 = this.ua.indexOf("Android 4") != -1;
		this.AndroidSmartPhone = this.Android && this.ua.indexOf("Mobile") != -1 && this.ua.indexOf("A1_07") == -1 && this.ua.indexOf("SC-01C") == -1;
		this.AndroidTablet = this.Android && (this.ua.indexOf("Mobile") == -1 || this.ua.indexOf("A1_07") != -1 || this.ua.indexOf("SC-01C") != -1);

		this.sp = this.iPhone || this.AndroidSmartPhone;
		this.tablet = this.iPad || this.AndroidTablet;
	};

	var isThisPc = new function() {
		this.ua = navigator.userAgent;

		this.windows = this.ua.indexOf("Windows NT") != -1;
		this.mac = this.ua.indexOf("Macintosh") != -1;

		this.ie11 = this.ua.indexOf("Trident/7") != -1;
		this.edge = this.ua.indexOf("Edge") != -1;
		this.firefox = this.ua.indexOf("Firefox") != -1;
		this.chrome = this.ua.indexOf("Chrome") != -1 && this.ua.indexOf("Edge") == -1;
		this.safari = this.ua.indexOf("Safari") != -1 && this.ua.indexOf("Chrome") == -1;
	};

	if(document.URL.match(isTargetPageRegex)) {
		// ONC: alm
		// DMG: gly / hlg / hmt / trz
		if (PRODUCT == 'alm' || PRODUCT == 'gly' || PRODUCT == 'hlg' || PRODUCT == 'hmt' || PRODUCT == 'trz'){
			document.write('<link rel="stylesheet" type="text/css" href="../../../common_dmg/brand/lilly/css/color.css">');
		}

		window.isThis = isThis;
		window.isThisPc = isThisPc;
	}
})();


if(document.URL.match(isTargetPageRegex)) {

	///////////////////////////////////////////////////////////////////////////////////
	/////
	///// textfit.js
	/////

	(function() {
		var isThis = new function() {
			this.ua = navigator.userAgent;

			this.iPhone = this.ua.indexOf("iPhone") != -1;
			this.iPad = this.ua.indexOf("iPad") != -1;
			this.iOS = this.iPhone || this.iPad;

			this.Android = this.ua.indexOf("Android") != -1;
			this.Android2 = this.ua.indexOf("Android 2") != -1;
			this.Android4 = this.ua.indexOf("Android 4") != -1;
			this.AndroidSmartPhone = this.Android && this.ua.indexOf("Mobile") != -1 && this.ua.indexOf("A1_07") == -1 && this.ua.indexOf("SC-01C") == -1;
			this.AndroidTablet = this.Android && (this.ua.indexOf("Mobile") == -1 || this.ua.indexOf("A1_07") != -1 || this.ua.indexOf("SC-01C") != -1);

			this.sp = this.iPhone || this.AndroidSmartPhone;
			this.tablet = this.iPad || this.AndroidTablet;
		};

		$(document).ready(function(){
			// 日本語で幅計算が狂うため、計算する前に文字数を調整する値を設定
			var adjust_val = 0.7;
			// JSに渡す初期値をオブジェクトとして設定
			var values = {
				line01: {
					maxFont: 26,
					minFont: 15,
				},
				line02: {
					maxFont: 15,
					minFont: 12,
				}
			};
			// 端末とラインそれぞれの最大値、最小値を設定
			var options = {
				pc:{
					line01:{
						maxFont: 26, // PC メインタイトル最大サイズ
						minFont: 20,　// PC メインタイトル最小サイズ
					},
					line02:{
						maxFont: 26, // PC サブタイトル最大サイズ
						minFont: 20, // PC サブタイトル最小サイズ
					}
				},
				tablet:{
					line01:{
						maxFont: 30, // タブレット メインタイトル最大サイズ
						minFont: 14, // タブレット メインタイトル最小サイズ
					},
					line02:{
						maxFont: 20, // タブレット サブタイトル最大サイズ
						minFont: 14, // タブレット サブタイトル最小サイズ
					}
				},
				sp: {
					line01:{
						maxFont: 20, // スマホ メインタイトル最大サイズ
						minFont: 8, // スマホ メインタイトル最小サイズ
					},
					line02:{
						maxFont: 13, // スマホ サブタイトル最大サイズ
						minFont: 8, // スマホ サブタイトル最小サイズ
					}
				},
			};

			if (isThis.sp) { // SP
				values = {
					line01: {
						maxFont: options.sp.line01.maxFont,
						minFont: options.sp.line01.minFont,
					},
					line02: {
						maxFont: options.sp.line02.maxFont,
						minFont: options.sp.line02.minFont,
					}
				};
			} else if (isThis.tablet) { //Tablet
				values = {
					line01: {
						maxFont: options.tablet.line01.maxFont,
						minFont: options.tablet.line01.minFont,
					},
					line02: {
						maxFont: options.tablet.line02.maxFont,
						minFont: options.tablet.line02.minFont,
					}
				};
			} else { //PC
				values = {
					line01: {
						maxFont: options.pc.line01.maxFont,
						minFont: options.pc.line01.minFont,
					},
					line02: {
						maxFont: options.pc.line02.maxFont,
						minFont: options.pc.line02.minFont,
					}
				};
			}

			var $text01 = $('#videoTitle01');
			var $text02 = $('#videoTitle02');

			function countLineLength(target) {
				var fullText = target.html();
				var lineText = fullText.replace(/<br(.*)>(.*)/g, '');
				var lineLength = lineText.length + adjust_val;
				return lineLength;
			}

			$text01.flowtype({
				maximum: 9999,
				minimum: 1,
				maxFont: values.line01.maxFont,
				minFont: values.line01.minFont,
				fontRatio: countLineLength($text01)
			});

			$text02.flowtype({
				maximum: 9999,
				minimum: 1,
				maxFont: values.line02.maxFont,
				minFont: values.line02.minFont,
				fontRatio: countLineLength($text02)
			});
		});

	})();

	///////////////////////////////////////////////////////////////////////////////////
	/////
	///// index.js
	/////

	(function() {
		var player;

		var mediaInfo = {
			speedMode: 'normal',
			currentTime: 0,
			normalSpeedModeText: '通常の速度に戻す<br>（1.0倍）',
			highSpeedModeText: '倍速再生で視聴する<br>（1.3倍）',
			highSpeedRate: 1.3,
			rewindTime: 1
		};

		$(function() {
			if($('#eqPlayer').length){
				$('#speedChange').html(mediaInfo.highSpeedModeText).addClass('is-normal');
				mediaInfo.mid = $('#eqPlayer').attr('data-normal-speed-mid');
				mediaInfo.hsMid = $('#eqPlayer').attr('data-high-speed-mid');

				setPlayer(mediaInfo.mid, 0);
				setSpeedChangeClickEvent();
			}
		});

		function setPlayer(mid, startTime, autoplay) {
			var ip = autoplay ? "on" : "off";

			player = jstream_t3.PlayerFactoryIF.create({
				b: "eqc393ggdp.eq.webcdn.stream.ne.jp/www50/eqc393ggdp/jmc_pub/jmc_swf/player/",  // JCWテスト用
				c: "Mjc3Nw==",  // JCWテスト用
				// b: "eqb673ulpm.eq.webcdn.stream.ne.jp/www50/eqb673ulpm/jmc_pub/jmc_swf/player/",
				// c: "MjA1Mg==",
				m: mid,
				t: startTime,
				s: {
					dq: "0",
					el: "off",
					il: "off",
					ip: "on",
					sn: "",
					tg: "off",
					ti: "off",
					rp: 'fit',
					ip: ip
				}
			}, "eqPlayer");

			EQP.control({ mid: mid, eid: EVENTID }, player);
		}

		function setSpeedChangeClickEvent() {
			$('#speedChange').on('click', function() {
				var playerState = player.accessor.state;
				if (playerState !== 'landing' && playerState !== 'playing' &&
					playerState !== 'paused' && playerState !== 'complete') {
					return false;
				}

				mediaInfo.currentState = playerState;

				if (mediaInfo.speedMode === 'normal') {
					mediaInfo.currentTime = player.accessor.getCurrentTime();
					mediaInfo.speedMode = 'highSpeed';
				} else {
					mediaInfo.currentTime = player.accessor.getCurrentTime() * mediaInfo.highSpeedRate;
					mediaInfo.speedMode = 'normal';
				}
				changeSpeed();
				return false;
			});
		}

		function changeSpeed() {
			var mid, text, startTime, className;
			if (mediaInfo.speedMode === 'normal') {
				mid = mediaInfo.mid;
				text = mediaInfo.highSpeedModeText;
				startTime = mediaInfo.currentTime - mediaInfo.rewindTime;
				className = mediaInfo.speedMode;
			} else {
				mid = mediaInfo.hsMid;
				text = mediaInfo.normalSpeedModeText;
				startTime = (mediaInfo.currentTime - mediaInfo.rewindTime) / mediaInfo.highSpeedRate;
				className = mediaInfo.speedMode;
			}

			if (startTime < 0) {
				startTime = 0;
			}

			$('#speedChange').html(text).removeClass().addClass('is-' + className);

			var autoplay = mediaInfo.currentState === 'playing' && (isThisPc.windows && (isThisPc.edge || isThisPc.firefox || isThisPc.ie11) ? true : false);

			setPlayer(mid, startTime, autoplay);
		}
	})();

}
