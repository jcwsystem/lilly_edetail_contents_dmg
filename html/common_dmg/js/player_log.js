var EQP = {
	loggingURL: "//tacos.co3.co.jp/users/lilly_veeva_vod/api2/set_playerlog.aspx",

	totalTime: 0,
	prevTime: 0,
	duration: 0,
	flag: {
		playFromHead: true
	},

	logPoint: [
		{id: "1", val: 11.1},
		{id: "2", val: 22.2},
		{id: "3", val: 33.3},
		{id: "4", val: 44.4},
		{id: "5", val: 55.5},
		{id: "6", val: 66.6},
		{id: "7", val: 77.7},
		{id: "8", val: 88.8},
		{id: "9", val: 99.9}
	],

	reset: function() {
		EQP.totalTime = 0;
		EQP.prevTime = 0;
		EQP.duration = 0;
		EQP.flag.playFromHead = true;
	},

	// var params = {
	// 	mid: MOVIEVOD,
	// 	eid: EVENTID,
	// }
	control: function(params, player) {
		EQP.reset();

		EQP.mid = params.mid;
		EQP.eid = params.eid;
		EQP.player = player;

		EQP.player.accessor.addEventListener("landing", function() {
			EQP.player.accessor.addEventListener("playing", EQP.playing);
			EQP.player.accessor.addEventListener("progress", EQP.progress);
			EQP.player.accessor.addEventListener("complete", EQP.complete);
		});
	},

	playing: function() {
		var initPlaying = function() {
			if (EQP.flag.playFromHead) {
				// 総尺取得
				EQP.duration = EQP.player.accessor.getTotalTime();

				// 総尺が取得できない場合を考慮
				if (EQP.duration > 1) {
					// log送信時間計算
					for (var i = 0; i <  EQP.logPoint.length; i++) {
						EQP.logPoint[i].time = EQP.duration * (EQP.logPoint[i].val / 100);
					}

					// 初回ログ送信
					EQP.logging("s");
					EQP.flag.playFromHead = false;
				} else {
					// 再実行
					setTimeout("initPlaying()", 1000);
				}
			}
		};

		initPlaying();
	},

	progress: function() {
		var i;
		var val1, val2;

		if (!EQP.flag.playFromHead) {
			if (EQP.duration > 1) {
				var currentTime = Math.floor(EQP.player.accessor.getCurrentTime());
				if (Math.abs(currentTime - EQP.prevTime) <= 2) {
					if (currentTime - EQP.prevTime == 1) {
						for (i = 0; i <  EQP.logPoint.length; i++) {
							val1 = EQP.logPoint[i].time - EQP.prevTime;
							val2 = EQP.logPoint[i].time - currentTime;
							if (val1 * val2 < 0 || val2 == 0) {
								EQP.logging(EQP.logPoint[i].id);
								break;
							}
						}

						EQP.prevTime = currentTime;
						EQP.totalTime += 1;
						//console.log("prevTime: " + EQP.prevTime + " totalTime: " + EQP.totalTime);

					}
				} else {
					EQP.prevTime = currentTime;
				}
			}
		}
	},

	complete: function() {
		EQP.logging("e");
		EQP.flag.playFromHead = true;
		EQP.totalTime = 0;
	},

	logging: function(pos) {
		function timeStr() {
			var dd = new Date();
			return dd.getFullYear() + "/" + (dd.getMonth() + 1) + "/" + dd.getDate() + " " + dd.getHours() + ":" + dd.getMinutes() + ":" + dd.getSeconds();
		}

		var urlParam = EQP.getUrlQuery();

		var logData = {
			mid: EQP.mid,
			pos: pos,
			time: encodeURIComponent(timeStr()),
			len: EQP.totalTime,
			// totallen: parseInt(EQP.duration, 10),
			eventid: EQP.eid,
			op4: urlParam.ref ? urlParam.ref : ''
		};
		//alert("uni: " + decodeURIComponent(logData.uni) + "　pos: " + logData.pos+ "　time: " + decodeURIComponent(logData.time) + "　len 総視聴時間: " + logData.len + "　mid 動画ID: " + logData.mid);
		//console.log("uni: " + decodeURIComponent(logData.uni) + "　pos: " + logData.pos+ "　time: " + decodeURIComponent(logData.time) + "　len 総視聴時間: " + logData.len + "　mid 動画ID: " + logData.mid);

		// Logging実行部分
		$.ajax({
			type: "GET",
			url: EQP.loggingURL,
			dataType: "text",
			data: logData,
			error: function() {
				//alert("エラー");
			},
			success: function() {
				//alert("成功");
			}
		});
		//
	},

	getUrlQuery: function() {
		var search = decodeURI(window.location.search.substring(1));
		var params = search.split("&");

		var obj = {};

		params.forEach(function(param) {
			var tmp = param.split("=");

			var key = tmp[0] ? tmp[0] : null;
			var value = tmp[1] ? tmp[1] : null;

			if (key && value) {
				obj[key] = value;
			}
		});

		return obj;
	}
}
