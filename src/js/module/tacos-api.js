// import $ from 'jquery';
import config from '../config';
import {
	getUrlQuery,
} from './common';

class TacosApi {
	constructor() {
		this.baseUrl = 'https://tacos.co3.co.jp/users/'+ config.tacos.apiDir;
		this.apiBaseUrl = this.baseUrl + '/api2/';

		// return json
		this.json;

		// リザルトコード（配列）
		this.resultCode = {
			ok: ['0000']
		};

		this.errorMessage = {
			api: 'AJAX通信エラー(API)',
			tacos: 'AJAX通信エラー(TACOS)',
		};

		this.api = {
			// getUserInfo: { url: this.apiBaseUrl + '', type: 'GET', dataType: 'jsonp' },
			getUserInfo: { url: this.apiBaseUrl + 'get_userinfo.aspx' },
			setClickLog: { url: this.apiBaseUrl + 'set_clicklog.aspx' },
			setBookmark: { url: this.apiBaseUrl + 'set_bookmark.aspx' },
			getBookmark: { url: this.apiBaseUrl + 'get_bookmark.aspx' },
			setLiveLog: { url: this.baseUrl + '/authdir/livelog.aspx', dataType: 'text' },
		};

		// 定数
		this.bookmark = {
			add: {
				type: '1',
				text: 'bookmarkon_',
			},
			delete: {
				type: '2',
				text: 'bookmarkoff_',
			}
		};

		// デバッグモード
		// queryで判断 userid=0があるときだけ、uid=0を渡してAPI機能をデバッグモードにする
		this.debugMode = getUrlQuery().debugUser === "0";
	}

	// get loginUser() {
	// 	return this.json['checkkey1'] ? this.json['checkkey1'] : null;
	// }

	request(api, param) {
		const d = new $.Deferred();

		$.ajax({
			type: api.type ? api.type : 'GET',
			url: api.url,
			data: param,
			cache: false,
			dataType: api.dataType ? api.dataType : 'jsonp',
			timeout: 10000,
		})
		.done((json) => {
			if (json.resultcode && $.inArray(json.resultcode, this.resultCode.ok) != -1) {
				// OK
				this.json = json;
				d.resolve(json);
			} else {
				// NG
				console.error(this.errorMessage.tacos, json.resultcode);
				d.reject();
			}
		})
		.fail((xhr, status, error) => {
			console.error(this.errorMessage.api, xhr, status, error);
			d.reject();
		})

		return d.promise();
	}

	getApiParam(param = {}) {
		// デバックモード時のパラメータ追加
		const debugModeParam = this.debugMode ? { uid: 0 } : {};
		return Object.assign(param, debugModeParam);
	}

	getUserInfo() {
		// リストに表示しないアイテムのパラメータ取得
		// デバッグ用dridの設定
		const drid = this.debugMode ? {drid: "0018000000uWIMiAAO"}: {};
		// 表示しないリストの領域を指定。未指定の際はblacklistのパラメータは応答しない（取得しない）
		const dep = {dep: "dmg"};
		// 上記のパラメータをthis.getApiParam()にマージして、パラメータ作成
		const blacklistParam = Object.assign(this.getApiParam(), dep, drid);

		return this.request(this.api.getUserInfo, blacklistParam);
	}

	// setPlayerLog() {
	// 	const url = this.apiBaseUrl + 'set_playerlog.aspx';
	// }

	setLiveLog(param) {
		return this.request(this.api.setLiveLog, param);
	}

	// setClickLog(param) {
	// 	console.log(param);
	// }

	setBookmark(param) {
		return this.request(this.api.setBookmark, this.getApiParam(param));
	}

	getBookmark() {
		return this.request(this.api.getBookmark, this.getApiParam());
	}

	setDoclogUrl(path) {
		return this.apiBaseUrl + 'set_doclog.aspx?' + 'doc_url=' + encodeURIComponent(path);
	}

}

export default new TacosApi();