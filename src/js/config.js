const config = {

	path: {
		xml: 'common_dmg/xml/config.xml',
	},

	tacos: {
		apiDir: 'lilly_veeva_vod',
	},

	// TODO:デバッグモード 提出時要確認
	eq: {
		token: 'OEREMTdCNkQ1RTFENjQ5OUZDQzVCRDQyMDk0NjA3RTM=', // JCWテスト用
		// token: 'MDM4MDJFNDE5QkZEMjkxMzBFODYxM0ExMEFENEQ1ODk=',
		url: 'https://api01-platform.stream.co.jp/apiservice/getMediaByParam/',
		status: {
			ok: '2000',
		},
		keywords: {

		},
		maxCount: 1000,
	},

	displayCount: {
		videoList: {
			col: 4,
			max: 16,
		},
		relatedVideoList: {
			pc: 5,
			sp: 2,
		},
	},

	breakPoint: 768,
};

export default config;