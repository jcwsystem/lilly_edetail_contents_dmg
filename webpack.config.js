const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FixStyleOnlyEntriesPlugin = require("webpack-fix-style-only-entries");

// const isProd = process.env.NODE_ENV === 'production';
// console.log(isProd);

const globule = require('globule');

// 対象ファイル検索用
const getEntriesList = (targetTypes, srcBase = `${__dirname}/src`) => {
	const entriesList = {};
	for(const [ srcType, targetType ] of Object.entries(targetTypes)) {
		const filesMatched = globule.find([`**/*.${srcType}`, `!**/_*.${srcType}`], { srcBase: srcBase });

		for(const srcName of filesMatched) {
			const targetName = srcName.replace(new RegExp(`.${srcType}$`, 'i'), `.${targetType}`);
			entriesList[targetName] = path.resolve(srcBase, srcName);
		}
	}
	return entriesList;
}

const app = {
	// モード値を production に設定すると最適化された状態で、
	// development に設定するとソースマップ有効でJSファイルが出力される
	mode: 'development',

	// ローカル開発用環境を立ち上げる
	// 実行時にブラウザが自動的に localhost を開く
	devServer: {
		contentBase: "html",
		open: true
	},

	// エントリーポイント
	entry: {
		// js
		'list': path.resolve(__dirname, './src/js/list.js'),
		'mylist': path.resolve(__dirname, './src/js/mylist.js'),
		'player': path.resolve(__dirname, './src/js/player.js'),
		// css
		'style': path.resolve(__dirname, './src/css/style.scss'),
		'style_player': path.resolve(__dirname, './src/css/player.scss'),
		// 'plugin': path.resolve(__dirname, './src/css/plugin.scss'),
	},

	// ファイルの出力設定
	output: {
		path: path.resolve(__dirname, './html'),
		filename: 'common_dmg/js/[name].js'
	},

	externals: {
		$: 'jQuery',
	},

	// 設定
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: path.resolve(__dirname, './node_modules'),
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: [
								[
									'@babel/preset-env',
									{
										'useBuiltIns': 'usage',
										'corejs': 3
									}
								],
							]
						}
					}
				]
			},
			// {
			// 	test: /\.(jpe?g|png|gif|svg)$/,
			// 	use: {
			// 		loader: 'file-loader',
			// 		options: {
			// 			name: '[name].[ext]',
			// 			outputPath: 'images/',
			// 			publicPath: path => '../' + path,
			// 			// esModule: false,
			// 		}
			// 	}
			// },
			{
				test: /\.ejs$/i,
				use: [
					{
						loader: 'html-loader',
						options: {
							minimize: false,
							attributes: false
						}
					},
					{
						loader: 'ejs-plain-loader'
					}
				]
			},
			{
				test: /\.(sa|sc|c)ss$/,
				exclude: path.resolve(__dirname, './node_modules'),
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							url: false,
							// minimize: true
						}
					},
					{
						loader: 'postcss-loader',
						options: {
							postcssOptions: {
								plugins: [
									'autoprefixer',
									[
										'postcss-sort-media-queries',
										{
											sort: 'desktop-first',
										}
									]
								]
							}
						}
					},
					'sass-loader'
				]
			}
		]
	},

	plugins: [
		new FixStyleOnlyEntriesPlugin(),
		new MiniCssExtractPlugin({
			filename: 'common_dmg/css/[name].css',
		})
	],

	// devtool: isProd ? false : 'source-map'
};

// ejsの対象ファイル追加
for(const [ targetName, srcName ] of Object.entries(getEntriesList({ ejs: 'html' }, path.resolve(__dirname, './src/html/page')))) {
  app.plugins.push(new HtmlWebpackPlugin({
    filename : targetName,
		template : srcName,
		inject: false,
		// minify: {
		// 	collapseWhitespace: true,
		// 	removeComments: true,
		// 	removeRedundantAttributes: true,
		// 	removeScriptTypeAttributes: true,
		// 	removeStyleLinkTypeAttributes: true,
		// 	useShortDoctype: true,
		// },
		minify: true
  }));
}

module.exports = app;